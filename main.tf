terraform {
  required_version = ">= 1.0.8"

  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
}

resource "docker_image" "server_test" {
  name         = "server_test"
  keep_locally = false
  build {
    path = "./"
    label = {
      author : "Cristiano"
    }
    force_remove = true
  }
}

resource "docker_network" "private_network" {
  name     = "catori-plugins"
  driver   = "bridge"
  internal = false
  check_duplicate = true
}

resource "docker_volume" "mongodb_volume" {
  name = "mongodb_volume"
}

resource "docker_volume" "postgres_volume" {
  name = "postgres_volume"
}

resource "docker_container" "server_test" {
  image = "server_test"
  name  = "server_test"
  ports {
    internal = 3000
    external = 3000
  }
  networks_advanced {
    name = "catori-plugins"
  }
  env = [
    "JWT_SECRET=",
    "BB_URL=",
    "BB_USER=",
    "BB_PASSWORD=",
    "MONGO_DB_SERVER=mongodb://root:jedi@mongodb:27017"
  ]
}

resource "docker_container" "postgres" {
  image = "postgres"
  name  = "postgres"
  ports {
    internal = 5432
    external = 5432
  }
  networks_advanced {
    name = "catori-plugins"
  }
  env = [
    "POSTGRES_USER=root",
    "POSTGRES_PASSWORD=jedi",
  ]
  volumes {
    volume_name    = "postgres_volume"
    container_path = "/data/db"
  }
}

