
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: '164.92.186.188',
      port: 5432,
      username: 'postgres',
      password: '123456',
      database: 'postgres',
      entities: [],
      synchronize: true,
    }),
  ],
})
export class AppModule { }